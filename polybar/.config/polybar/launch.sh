#!/usr/bin/env bash

DIR="$HOME/.config/polybar/"

pkill polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch the bar
polybar -q main -c "$DIR"/config.ini &
